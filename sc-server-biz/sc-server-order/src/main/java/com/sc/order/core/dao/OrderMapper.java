package com.sc.order.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.order.entity.order.Order;

/**
 * @author: wust
 * @date: 2020-07-15 14:14:39
 * @description:
 */
public interface OrderMapper extends IBaseMapper<Order>{
}