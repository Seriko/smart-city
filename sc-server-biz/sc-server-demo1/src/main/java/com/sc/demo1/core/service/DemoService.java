package com.sc.demo1.core.service;

import com.sc.demo1.entity.demo.Demo;
import com.sc.common.service.BaseService;

public interface DemoService extends BaseService<Demo> {
}
