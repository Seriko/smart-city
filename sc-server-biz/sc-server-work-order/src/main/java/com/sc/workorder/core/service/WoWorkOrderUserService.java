package com.sc.workorder.core.service;

import com.sc.common.service.BaseService;
import com.sc.workorder.entity.WoWorkOrderUser;

/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
public interface WoWorkOrderUserService extends BaseService<WoWorkOrderUser>{
}
