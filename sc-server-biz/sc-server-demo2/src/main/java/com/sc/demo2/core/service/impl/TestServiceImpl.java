package com.sc.demo2.core.service.impl;

import com.sc.demo2.core.dao.TestMapper;
import com.sc.demo2.core.service.TestService;
import com.sc.demo2.entity.test.Test2;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-07-04 13:29:41
 * @description:
 */
@Service("testServiceImpl")
public class TestServiceImpl extends BaseServiceImpl<Test2> implements TestService {
    @Autowired
    private TestMapper testMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return testMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
