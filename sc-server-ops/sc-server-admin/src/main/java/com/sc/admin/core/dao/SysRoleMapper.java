package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.role.SysRole;


public interface SysRoleMapper   extends IBaseMapper<SysRole> {
}
