package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.datasource.detail.SysDataSourceDetail;

/**
 * @author: wust
 * @date: 2020-05-01 20:32:48
 * @description:
 */
public interface SysDataSourceDetailMapper extends IBaseMapper<SysDataSourceDetail> {
}