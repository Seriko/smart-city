/**
 * Created by wust on 2020-03-31 09:13:01
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;

import com.sc.admin.core.cache.RedisCacheLookupBo;
import com.sc.admin.core.dao.SysLookupMapper;
import com.sc.admin.core.xml.XMLAbstractResolver;
import com.sc.admin.core.xml.XMLDefinitionFactory;
import com.sc.admin.core.xml.factory.XMLLookupFactory;
import com.sc.common.service.InitializtionService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author: wust
 * @date: Created in 2020-03-31 09:13:01
 * @description:
 *
 */
@Order(1)
@Service
public class Init4LookupServiceImpl implements InitializtionService {
    @Autowired
    private SysLookupMapper sysLookupMapper;

    @Autowired
    private RedisCacheLookupBo redisCacheLookupBo;

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void init() {
        XMLDefinitionFactory xmlLookupFactory = new XMLLookupFactory();
        XMLAbstractResolver resolver = xmlLookupFactory.createXMLResolver();
        Map<String, List> resultMap =  resolver.getResult();

        sysLookupMapper.deleteAll();

        List list = resultMap.get("resultList");
        if(CollectionUtils.isNotEmpty(list)){
            sysLookupMapper.insertList(list);

            redisCacheLookupBo.init();
        }
    }
}
