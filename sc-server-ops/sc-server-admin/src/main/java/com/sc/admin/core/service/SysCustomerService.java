package com.sc.admin.core.service;

import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.service.BaseService;

/**
 * @author: wust
 * @date: 2019-12-27 11:37:51
 * @description:
 */
public interface SysCustomerService extends BaseService<SysCustomer>{
    /**
     * 客户注册
     * @param account
     * @param customer
     */
    void registerAccount(SysAccount account, SysCustomer customer);
}
