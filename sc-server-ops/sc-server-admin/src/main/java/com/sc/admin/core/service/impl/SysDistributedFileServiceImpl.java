package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysDistributedFileMapper;
import com.sc.admin.core.service.SysDistributedFileService;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-03-08 15:25:30
 * @description:
 */
@Service("sysDistributedFileServiceImpl")
public class SysDistributedFileServiceImpl extends BaseServiceImpl<SysDistributedFile> implements SysDistributedFileService{
    @Autowired
    private SysDistributedFileMapper sysDistributedFileMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysDistributedFileMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
