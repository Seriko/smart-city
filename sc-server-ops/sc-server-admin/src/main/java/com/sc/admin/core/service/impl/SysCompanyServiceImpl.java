package com.sc.admin.core.service.impl;


import com.sc.admin.core.dao.SysCompanyMapper;
import com.sc.admin.core.service.SysCompanyService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wust on 2019/6/3.
 */
@Service("sysCompanyServiceImpl")
public class SysCompanyServiceImpl  extends BaseServiceImpl<SysCompany> implements SysCompanyService {
    @Autowired
    private SysCompanyMapper sysCompanyMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysCompanyMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
