package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.importexport.SysImportExport;

/**
 * Created by wust on 2019/5/20.
 */
public interface SysImportExportMapper  extends IBaseMapper<SysImportExport> {
}
