package com.sc.generatorcode.invoker.base;

/**
 * @author ：wust
 * Date   2018/9/5
 */
public abstract class AbstractBuilder {

    public abstract Invoker build();

    public boolean isParametersValid() {
        try {
            checkBeforeBuild();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public abstract void checkBeforeBuild() throws Exception;

}
