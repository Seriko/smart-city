package com.sc.easyexcel.definition.xml;/**
 * Created by wust on 2017/5/8.
 */

import cn.hutool.core.io.resource.ClassPathResource;
import com.sc.easyexcel.exception.EasyExcelException;
import com.sc.easyexcel.definition.ExcelDefinitionReader;
import org.xml.sax.SAXException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * Function:基于XML的配置注册抽象类
 * Reason:
 * Date:2017/5/8
 * @author wust
 */
public abstract class XMLExcelDefinitionReader implements ExcelDefinitionReader {

    protected String xsdPath;

    protected String xmlPath;


    public XMLExcelDefinitionReader(String xsdPath, String xmlPath){
        this.xsdPath = xsdPath;
        this.xmlPath = xmlPath;
        validateXML();
    }



    /**
     * 验证XML是否按照规范来编写
     * @throws Exception
     */
    protected void validateXML() throws EasyExcelException {
        try{
            SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            ClassPathResource classPathResourceXsd = new ClassPathResource(xsdPath);
            InputStream inputStreamXsd = classPathResourceXsd.getStream();
            Source sourceXsd = new StreamSource(inputStreamXsd);
            Schema schema = schemaFactory.newSchema(sourceXsd);
            Validator validator = schema.newValidator();

            ClassPathResource classPathResourceXml = new ClassPathResource(xmlPath);
            InputStream inputStreamXml = classPathResourceXml.getStream();
            Source source = new StreamSource(inputStreamXml);
            validator.validate(source);
        } catch (FileNotFoundException e) {
            throw new EasyExcelException("找不到[" + xsdPath + "]或[" + xmlPath + "]文件",e);
        } catch (SAXException e) {
            throw new EasyExcelException("XML[" + xmlPath + "]不按照XSD[" + xsdPath + "]规范来编写",e);
        } catch (IOException e) {
            throw new EasyExcelException("XML[" + xmlPath + "]不按照XSD[" + xsdPath + "]规范来编写",e);
        }
    }
}
