
package com.sc.common.entity;



import com.sc.common.util.MyGenId;
import lombok.Data;
import lombok.ToString;
import tk.mybatis.mapper.annotation.KeySql;
import javax.persistence.Id;
import java.util.Date;

/**
 * 基础对象
 * 2019年2月22日 11:35:44
 * @author wust
 */
@ToString
@Data
public class BaseEntity implements java.io.Serializable {
    private static final long serialVersionUID = -7114772512426125749L;

    /**
     * 主键
     */
    @Id
    @KeySql(genId = MyGenId.class)
    protected Long id;
    /**
     * 创建人id
     **/
    protected Long createrId;
    /**
     * 创建人名称
     **/
    protected String createrName;
    /**
     * 创建时间
     **/
    protected Date createTime;
    /**
     * 修改人id
     **/
    protected Long modifyId;
    /**
     * 修改人名称
     **/
    protected String modifyName;
    /**
     * 修改时间
     **/
    protected Date modifyTime;
    /**
     * 是否已经删除：1是，0否
     */
    protected Integer isDeleted = 0;
}

