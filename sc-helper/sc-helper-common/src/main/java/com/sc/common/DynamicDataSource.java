package com.sc.common;


import com.alibaba.druid.pool.DruidDataSource;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by wust on 2019/6/20.
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    private static Log logger = LogFactory.getLog(DynamicDataSource.class);

    private static Map<Object, Object> dataSourceMap = new ConcurrentHashMap<>();

    @Override
    protected Object determineCurrentLookupKey() {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        if(ctx.getDataSourceId() == null){
            return null;
        }

        lookupAndCreateDataSource(ctx.getDataSourceId());
        return ctx.getDataSourceId();
    }

    private void lookupAndCreateDataSource(String dataSourceId) {
        if(!dataSourceMap.containsKey(dataSourceId)){
            SysDataSource sysDataSource =  DataDictionaryUtil.getApplicationPOByPrimaryKey(dataSourceId,SysDataSource.class);
            if(sysDataSource != null){
                DruidDataSource druidDataSource = new DruidDataSource();
                druidDataSource.setUrl(sysDataSource.getJdbcUrl());
                druidDataSource.setUsername(sysDataSource.getJdbcUsername());
                druidDataSource.setPassword(sysDataSource.getJdbcPassword());
                druidDataSource.setDriverClassName(sysDataSource.getJdbcDriver());
                createDataSource(dataSourceId,druidDataSource);
            }else{
                logger.error("创建数据源失败，缓存里面没有发现数据源，数据源ID["+dataSourceId+"]");
                throw new BusinessException("创建数据源失败，缓存里面没有发现数据源，数据源ID["+dataSourceId+"]");
            }
        }
    }


    public DataSource createDataSource(String id,DruidDataSource dataSource) {
        if (dataSource != null) {
            dataSourceMap.put(id, dataSource);
            super.setTargetDataSources(dataSourceMap);
            super.afterPropertiesSet();
            return dataSource;
        }
        return null;
    }

}
