package com.sc.common.mapper;


import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DataAccessException;
import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.common.Mapper;
import java.util.List;


public interface IBaseMapper<T>  extends Mapper<T>, InsertListMapper<T> {
    List<?> selectByPageNumSize(
                    @Param("search")Object search,
                    @Param("pageNum") int pageNum,
                    @Param("pageSize") int pageSize);

    int batchUpdate(List<T> entities) throws DataAccessException;


    int batchDelete(List<Long> keys) throws DataAccessException;
}
