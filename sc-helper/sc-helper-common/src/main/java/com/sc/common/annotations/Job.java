package com.sc.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识为Job的注解
 * Created by wust on 2019/6/14.
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Job {
    String jobName();
    String jobDescription();
    String jobGroupName();
    String cronExpression();
}
