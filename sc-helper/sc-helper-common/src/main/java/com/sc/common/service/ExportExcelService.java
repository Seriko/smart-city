package com.sc.common.service;


import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;

/**
 * Created by wust on 2019/9/7.
 */
public interface ExportExcelService {

    WebResponseDto export(JSONObject jsonObject);
}
