package com.sc.common.interceptors.context;

import cn.hutool.core.convert.Convert;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.CommonUtil;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.SpringContextHolder;
import com.sc.common.util.WebUtil;
import com.sc.common.util.cache.SpringRedisTools;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:48
 * @description：前端请求走这里设置上下文
 * @version:
 */
public class WebStrategy implements IStrategy {
    private static SpringRedisTools springRedisTools;

    @Override
    public void setDefaultBusinessContext() {
        String tokenBase64 = MyStringUtils.null2String(WebUtil.getHeader(ApplicationEnum.X_AUTH_TOKEN.getStringValue()));
        if (MyStringUtils.isBlank(tokenBase64)) {
            tokenBase64 = WebUtil.getParameter(ApplicationEnum.X_AUTH_TOKEN.getStringValue());
        }

        if(MyStringUtils.isBlank(tokenBase64)){
            throw new BusinessException("非法的请求：缺少令牌");
        }

        String redisKey = null;
        try {
            redisKey = new String(Base64.decodeBase64(tokenBase64), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("token解码失败");
        }

        DefaultBusinessContext.getContext().setxAuthToken(tokenBase64);

        DefaultBusinessContext.getContext().setRedisKey(redisKey);

        Map mapValue = getRedisSpringBean().getMap(redisKey);
        if (mapValue != null) {
            SysAccount account = (SysAccount) mapValue.get("account");

            DefaultBusinessContext.getContext().setUser(null);

            DefaultBusinessContext.getContext().setAccountId(account.getId());

            DefaultBusinessContext.getContext().setAccountCode(account.getAccountCode());

            DefaultBusinessContext.getContext().setAccountName(account.getAccountName());

            DefaultBusinessContext.getContext().setAccountType(account.getType());

            DefaultBusinessContext.getContext().setAgentId(Convert.toLong(mapValue.get("agentId")));

            DefaultBusinessContext.getContext().setParentCompanyId(Convert.toLong(mapValue.get("parentCompanyId")));

            DefaultBusinessContext.getContext().setBranchCompanyId(Convert.toLong(mapValue.get("companyId")));

            DefaultBusinessContext.getContext().setProjectId(Convert.toLong(mapValue.get("projectId")));
            if (CommonUtil.isStaffAccount(account.getType())) { // 员工
                if (mapValue.get("user") != null) {
                    SysUser user = (SysUser) mapValue.get("user");
                    DefaultBusinessContext.getContext().setUser(user);
                }
            }


            /**
             * 设置语言环境
             */
            String localeStr = Convert.toStr(mapValue.get(ApplicationEnum.X_LOCALE.getStringValue()));
            Locale locale = new Locale(localeStr.split("-")[0], localeStr.split("-")[1]);
            DefaultBusinessContext.getContext().setLocale(locale);
        }
    }


    private static SpringRedisTools getRedisSpringBean() {
        if (springRedisTools == null) {
            springRedisTools = SpringContextHolder.getBean("springRedisTools");
        }
        return springRedisTools;
    }
}
