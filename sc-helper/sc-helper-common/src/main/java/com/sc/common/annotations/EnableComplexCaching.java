package com.sc.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 除key=id之外的复杂缓存
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface EnableComplexCaching {
    // 依赖的持久化对象简名，如SysAccount。如果此缓存需要监听某数据的变化而同步缓存时，需要填写此属性值。
    String dependsOnPOSimpleName();
}
