package com.sc.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "white-list")
@Configuration
public class WhiteListProperties {
    private String[] loggedIn;
    private String[] noLogin;

    public String[] getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(String[] loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String[] getNoLogin() {
        return noLogin;
    }

    public void setNoLogin(String[] noLogin) {
        this.noLogin = noLogin;
    }
}
