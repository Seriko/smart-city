package com.sc.common.enums;

/**
 * 应用枚举
 * Created by wust on 2019/3/29.
 */
public enum RabbitMqOperationTypeEnum {

    OPR_INIT,
    OPR_ADD,
    OPR_BATCH_ADD,
    OPR_UPDATE,
    OPR_BATCH_UPDATE,
    OPR_DEL,
    OPR_BATCH_DEL,
    OPR_RESET_CACHE;

    RabbitMqOperationTypeEnum(){}
}
