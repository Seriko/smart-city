/**
 * Created by wust on 2019-10-30 14:56:14
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.interceptors.context;

import cn.hutool.core.convert.Convert;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.CommonUtil;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.SpringContextHolder;
import com.sc.common.util.WebUtil;
import com.sc.common.util.cache.SpringRedisTools;
import org.apache.commons.codec.binary.Base64;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;

/**
 * @author: wust
 * @date: Created in 2019-10-30 14:56:14
 * @description: app拦截器实现
 */
public class AppApiStrategy implements IStrategy {
    private static SpringRedisTools springRedisTools;

    @Override
    public void setDefaultBusinessContext() {
        DefaultBusinessContext.getContext().setDataSourceId(ApplicationEnum.DEFAULT.name());

        String tokenBase64 = WebUtil.getHeader(ApplicationEnum.X_AUTH_TOKEN.getStringValue());
        if (MyStringUtils.isBlank(tokenBase64)) {
            tokenBase64 = WebUtil.getParameter(ApplicationEnum.X_AUTH_TOKEN.getStringValue());
        }

        if(MyStringUtils.isBlank(tokenBase64)){
            throw new BusinessException("非法的请求：缺少令牌");
        }

        String redisKey = null;
        try {
            redisKey = new String(Base64.decodeBase64(tokenBase64), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("token解码失败");
        }


        DefaultBusinessContext.getContext().setxAuthToken(tokenBase64);

        DefaultBusinessContext.getContext().setRedisKey(redisKey);

        Map mapValue = getRedisSpringBean().getMap(redisKey);

        SysAccount account = (SysAccount) mapValue.get("account");

        DefaultBusinessContext.getContext().setAccountId(account.getId());

        DefaultBusinessContext.getContext().setAccountCode(account.getAccountCode());

        DefaultBusinessContext.getContext().setAccountName(account.getAccountName());

        DefaultBusinessContext.getContext().setAccountType(account.getType());

        DefaultBusinessContext.getContext().setUser(null);

        DefaultBusinessContext.getContext().setAccountId(account.getId());

        DefaultBusinessContext.getContext().setAccountCode(account.getAccountCode());

        DefaultBusinessContext.getContext().setAccountName(account.getAccountName());

        DefaultBusinessContext.getContext().setAccountType(account.getType());

        DefaultBusinessContext.getContext().setAgentId(Convert.toLong(mapValue.get("agentId")));

        DefaultBusinessContext.getContext().setParentCompanyId(Convert.toLong(mapValue.get("parentCompanyId")));

        DefaultBusinessContext.getContext().setBranchCompanyId(Convert.toLong(mapValue.get("companyId")));

        DefaultBusinessContext.getContext().setProjectId(Convert.toLong(mapValue.get("projectId")));
        if (CommonUtil.isStaffAccount(account.getType())) { // 员工
            if (mapValue.get("user") != null) {
                SysUser user = (SysUser) mapValue.get("user");
                DefaultBusinessContext.getContext().setUser(user);
            }
        }else if (CommonUtil.isCustomer(account.getType())) {
            if (mapValue.get("customer") != null) {
                SysCustomer customer = (SysCustomer) mapValue.get("customer");

                DefaultBusinessContext.getContext().setCustomer(customer);

                DefaultBusinessContext.getContext().setAgentId(customer.getAgentId());

                DefaultBusinessContext.getContext().setParentCompanyId(customer.getParentCompanyId());

                DefaultBusinessContext.getContext().setBranchCompanyId(customer.getBranchCompanyId());

                DefaultBusinessContext.getContext().setProjectId(customer.getProjectId());
            }else{
                DefaultBusinessContext.getContext().setCustomer(null);

                DefaultBusinessContext.getContext().setAgentId(null);

                DefaultBusinessContext.getContext().setParentCompanyId(null);

                DefaultBusinessContext.getContext().setBranchCompanyId(null);

                DefaultBusinessContext.getContext().setProjectId(null);
            }
        }else{
            DefaultBusinessContext.getContext().setUser(null);

            DefaultBusinessContext.getContext().setCustomer(null);

            DefaultBusinessContext.getContext().setAgentId(null);

            DefaultBusinessContext.getContext().setParentCompanyId(null);

            DefaultBusinessContext.getContext().setBranchCompanyId(null);

            DefaultBusinessContext.getContext().setProjectId(null);
        }



        /**
         * 设置语言环境
         */
        String localeStr = Convert.toStr(mapValue.get(ApplicationEnum.X_LOCALE.getStringValue()));
        Locale locale = new Locale(localeStr.split("-")[0], localeStr.split("-")[1]);
        DefaultBusinessContext.getContext().setLocale(locale);
    }


    private static SpringRedisTools getRedisSpringBean() {
        if (springRedisTools == null) {
            springRedisTools = SpringContextHolder.getBean("springRedisTools");
        }
        return springRedisTools;
    }
}
